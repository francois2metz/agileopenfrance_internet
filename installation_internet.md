# Internet libre pour les athénien-e-s

L'objectif est de décrire comment fournir une connexion wifi aux participants de l'agile open france.

## Salle Rétro-lounge
### Mise en place du raccordement internet

C'est par les antennes 4G et les cartes sims : bouyges, orange, free qu'internet sera disponible.
Les antennes sont collés à la vitre en direction de chez Schwartz.

```mermaid
graph TD;
  
Orange --> routeur;
Bouygues --> routeur;
Free --> routeur;
routeur --> switch;
switch --> rallonge_de_20m;
rallonge_de_20m --> lan_arnold;
switch --> antenne_wifi (ruckus_wireless);
switch --> controlleur_wifi;
```
Internet est maintenant disponible en wifi autour du rétro longe.

Le lan arnold est une prise ethernet branché au premier étage à droite (à coté de l'escalier) et raccordé par une prise de 20 mètre jusqu'aux équipements réseaux du retro-longe.

## accueil
On débranche le lan_arnold (wan sur le routeur arnold) afin d'éviter les conflits dhcp (2 serveurs sur le même réseau).
Le ```lan_arnold``` est branché sur un ```switch ruckus wireless```.

```mermaid
graph TD;

lan_arnold --> switch;
switch --> ruckus_wireless;
```

## Salle de brassage
On brasse la prise B14 sur le panel sur la deuxième rangée à partir du haut.

## Salle des marchés
Depuis la prise B14 de la salle des marchés, je branche un ```ruckus wireless```
Cela permet de proposer le wifi dans cette partie de l'hotel.
```mermaid
graph TD;

lan_arnold --> ruckus_wireless;
```